# Scope Graphics

This project was written over the course of a few days as a demo piece. The main functionality is to allow (using an Arduino and some basic electronics) a user to play the game 'Pong' on an oscilloscope.

The basic principle is that the two 6-bit ports on the Arduino Uno are manipulated rapidly to produce a variable voltage once channeled through a digital-to-analogue converter. These two voltages are used to deflect the point on the scope in X-Y mode.
