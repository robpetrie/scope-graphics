// Port D is X, Port B is Y
// 'Display' is effectively 63x63

#define SHAPE_ITERATIONS 50 // This controls how many times each shape is drawn per loop, tweaking this affects flicker and movement speed (kind of like framerate)

#define PLAYER_W 5
#define PLAYER_H 20
#define PLAYER_SPD 0.2

#define BALL_SIZE 1

// Some test shapes
byte centrePoint[][2] = {{32,32}};
byte centreBox[][2] = { {22, 22}, {22, 42}, {42, 42}, {42, 22}, {22, 22} };
byte boundingBox[][2] = { {0,0}, {0,63}, {63,63}, {63,0}, {0,0} };
byte centreVLine[][2] = { {32,0}, {32,63} };

byte shape[][2] = { {0,0}, {0,32}, {32,32}, {32,0} };
byte shape2[][2] = { {35,0}, {35,32}, {60,32}, {60,0} };

// EARS Logo
byte e[][2] = {{1, 43}, {1, 21}, {13, 21}, {13, 24}, {5, 24}, {5, 31}, {11, 31}, {11, 34}, {5, 34}, {5, 40}, {13, 40}, {13, 43}, {1, 43}, {1, 43}};
byte a1[][2] = {{13, 21}, {14, 22}, {14, 23}, {15, 24}, {15, 25}, {16, 27}, {16, 28}, {17, 30}, {18, 32}, {18, 33}, {19, 36}, {20, 38}, {21, 40}, {21, 41}, {22, 43}, {24, 43}, {25, 42}, {25, 40}, {26, 37}, {27, 35}, {28, 33}, {29, 31}, {29, 29}, {30, 27}, {30, 25}, {31, 24}, {31, 22}, {32, 21}, {28, 21}, {27, 22}, {27, 23}, {26, 25}, {26, 26}, {25, 27}, {25, 28}, {21, 28}, {20, 27}, {20, 26}, {19, 25}, {19, 24}, {18, 23}, {18, 21}, {18, 21}, {13, 21}};
byte a2[][2] = {{21, 31}, {22, 32}, {22, 33}, {22, 34}, {23, 36}, {23, 37}, {23, 35}, {23, 34}, {24, 33}, {24, 31}, {24, 31}, {21, 31}};
byte r1[][2] = {{33, 44}, {33, 21}, {37, 21}, {37, 30}, {38, 30}, {39, 28}, {41, 26}, {42, 24}, {42, 22}, {43, 21}, {46, 21}, {45, 24}, {45, 26}, {44, 27}, {43, 29}, {42, 31}, {41, 31}, {43, 32}, {44, 34}, {44, 36}, {45, 38}, {44, 40}, {43, 42}, {41, 43}, {39, 44}, {33, 44}};
byte r2[][2] = {{37, 33}, {37, 40}, {39, 41}, {40, 40}, {41, 39}, {42, 37}, {41, 34}, {39, 33}, {37, 33}};
byte s[][2] = {{48, 24}, {47, 21}, {50, 21}, {52, 21}, {54, 22}, {55, 23}, {57, 24}, {58, 27}, {59, 28}, {58, 30}, {57, 32}, {55, 33}, {53, 33}, {52, 35}, {51, 37}, {52, 39}, {53, 41}, {55, 42}, {58, 42}, {60, 42}, {60, 44}, {57, 44}, {54, 44}, {51, 43}, {50, 41}, {49, 38}, {49, 36}, {50, 33}, {52, 31}, {54, 30}, {56, 29}, {56, 28}, {55, 26}, {53, 24}, {50, 23}, {48, 24}};

byte numbers[10][11][2] = {
  {{0,0},{0,10},{5,10},{5,0},{0,0}}, // 0
  {{2,0},{2,10},{2,0},{2,10},{2,0},{2,10},{2,0},{2,10},{2,0},{2,10},{2,0}}, // 1
  {{5,0},{0,0},{0,5},{5,5},{5,10},{0,10},{5,10},{5,5},{0,5},{0,0},{5,0}}, // 2
  {{0,0},{5,0},{5,5},{0,5},{5,5},{5,10},{0,10},{5,10},{5,0}}, // 3
  {{5,0},{5,5},{0,5},{0,10},{0,5},{5,5},{5,10},{5,0},{5,10},{5,0},{5,10}}, // 4
  {{0,0},{5,0},{5,5},{0,5},{0,10},{5,10},{0,10},{0,5},{5,5},{5,0},{0,0}}, // 5
  {{5,10},{0,10},{0,0},{5,0},{5,5},{0,5},{5,5},{5,0},{0,0},{0,10},{5,10}}, // 6
  {{5,0},{5,10},{0,10},{5,10},{5,0},{5,10},{0,10},{5,10},{5,0},{5,10},{0,10}}, // 7
  {{0,0},{0,10},{5,10},{5,0},{0,0},{0,5},{5,5},{0,5},{5,5},{0,5},{5,5}}, // 8
  {{5,0},{5,10},{0,10},{0,5},{5,5},{5,0},{5,10},{0,10},{0,5},{5,5},{5,0}}  // 9  
};

// Define and initialise some info about the players
int player1XOffset = 4;
float player1YPos = 32;
int player1Score = 0;

int player2XOffset = 59;
float player2YPos = 32;
int player2Score = 0;

// Vars for the ball
float ballX = 32;
float ballY = 32;
float ballXSpd = 0.2;
float ballYSpd = 0.1;

void setup() {

  // Set pins 2 to 7 as output, leaving 0 and 1 as is (TX/RX)
  DDRD = DDRD | B11111100;

  // Set pins 8 to 13 as output, leaving the crystal pins as is
  DDRB = DDRB | B00111111;

  Serial.begin(9600);

}

void loop() {

  if( analogRead(A4) > 500 ) {
    playPong();
  } else {
    displayEARS();
  }

}

void displayEARS() {
  drawShape(e, sizeof(e)/sizeof(e[0]));
  drawShape(a1, sizeof(a1)/sizeof(a1[0]));
  drawShape(a2, sizeof(a2)/sizeof(a2[0])); 
  drawShape(r1, sizeof(r1)/sizeof(r1[0]));
  drawShape(r2, sizeof(r2)/sizeof(r2[0]));
  drawShape(s, sizeof(s)/sizeof(s[0]));
}

void playPong() {

  displayScore(1);
  displayScore(2);

  // Draw some static shapes that form the pitch
  drawShape(boundingBox, sizeof(boundingBox)/sizeof(boundingBox[0]));
  drawShape(centreVLine, sizeof(centreVLine)/sizeof(centreVLine[0]));

  handlePlayer1();
  handlePlayer2();
  handleBall();
}

void handlePlayer1() {

  // Calculate coords for the 4 corners of the paddle
  byte x0 = player1XOffset - (PLAYER_W/2);
  byte x1 = player1XOffset + (PLAYER_W/2);

  byte y0 = player1YPos - (PLAYER_H/2);
  byte y1 = player1YPos + (PLAYER_H/2);

  if ( analogRead(A0) > 500 && y1 < 64) {
    player1YPos += PLAYER_SPD;
    y0 = floor(player1YPos) - (PLAYER_H/2);
    y1 = floor(player1YPos) + (PLAYER_H/2);
  }

  if ( analogRead(A1) > 500 && y0 > 0) {
    player1YPos -= PLAYER_SPD;
    y0 = floor(player1YPos) - (PLAYER_H/2);
    y1 = floor(player1YPos) + (PLAYER_H/2);
  }

  byte points[][2] = {{x0, y0},{x0, y1},{x1, y1},{x1, y0}, {x0, y0}}; // Combine all the calculated points into an array
  
  drawShape(points, 5); // Send the points to the renderer
  
}

void handlePlayer2() {

  // Calculate coords for the 4 corners of the paddle
  byte x0 = player2XOffset - (PLAYER_W/2);
  byte x1 = player2XOffset + (PLAYER_W/2);

  byte y0 = player2YPos - (PLAYER_H/2);
  byte y1 = player2YPos + (PLAYER_H/2);

  if ( analogRead(A2) > 500 && y1 < 64) {
    player2YPos += PLAYER_SPD;
    y0 = floor(player2YPos) - (PLAYER_H/2);
    y1 = floor(player2YPos) + (PLAYER_H/2);
  }

  if ( analogRead(A3) > 500 && y0 > 0) {
    player2YPos -= PLAYER_SPD;
    y0 = floor(player2YPos) - (PLAYER_H/2);
    y1 = floor(player2YPos) + (PLAYER_H/2);
  }

  byte points[][2] = {{x0, y0},{x0, y1},{x1, y1},{x1, y0}, {x0, y0}}; // Combine all the calculated points into an array
  
  drawShape(points, 5); // Send the points to the renderer
  
}

void handleBall() {

  // Calculate the coords for the 4 corners of the ball
  byte x0 = ballX - (BALL_SIZE/2);
  byte x1 = ballX + (BALL_SIZE/2);

  byte y0 = ballY - (BALL_SIZE/2);
  byte y1 = ballY + (BALL_SIZE/2);


  // If the ball is outside the width of the screen, a change of x direction is needed
  if ( x0 < 1 ) {
    ballXSpd = -ballXSpd;
    player2Score++;
//    printScores();
  } else if (x1 > 64 ) {
    ballXSpd = -ballXSpd;
    player1Score++;
//    printScores();
  }

  // If the ball hits the top or bottom of the screen, a change of y direction is needed
  if ( y0 < 1 || y1 > 64 ) {
    ballYSpd = -ballYSpd;
  }

  // Check if the ball has collided with a paddle
  if ( (x0 < (player1XOffset + (PLAYER_W/2)) && ballXSpd < 0 && y0 > (player1YPos - (PLAYER_H/2)) && y1 < (player1YPos + (PLAYER_H/2)) ) || (x1 > (player2XOffset-(PLAYER_W/2)) && ballXSpd > 0 && y0 > (player2YPos - (PLAYER_H/2)) && y1 < (player2YPos + (PLAYER_H/2))) ) {
    ballXSpd = -ballXSpd;
  }

  ballX += ballXSpd;
  ballY += ballYSpd;

  // Re-calculate the coords for the 4 corners of the ball
  x0 = floor(ballX) - (BALL_SIZE/2);
  x1 = floor(ballX) + (BALL_SIZE/2);

  y0 = floor(ballY) - (BALL_SIZE/2);
  y1 = floor(ballY) + (BALL_SIZE/2);

  byte points[][2] = {{x0, y0},{x0, y1},{x1, y1},{x1, y0}, {x0, y0}}; // Combine all the calculated points into an array
  
  drawShape(points, 5); // Send the points to the renderer
  
}

void displayScore(int player) {

  int score = 0;
  int num = 0;
  int xOffset = 0;

  if ( player == 1 ) {

    score = player1Score;

    // Handle the special case of score = 0
    if( score == 0 ) drawNumber(num, 17-xOffset, 50);

    // Handle special case of score >99
    if ( score > 99 ) {
      drawNumber(9, 17, 50);
      drawNumber(9, 7, 50);
    }

    // Extract each digit of the number and draw them
    while ( score > 0 && score < 100 ) {
      
      num = score % 10;
      score = score / 10;

      drawNumber(num, 17-xOffset, 50);
  
      xOffset += 10; 
    }
    
  } else if ( player == 2 ) {

    score = player2Score;

    // Handle the special case of score = 0
    if( score == 0 ) drawNumber(num, 45-xOffset, 50);

    // Handle special case of score >99
    if ( score > 99 ) {
      drawNumber(9, 45, 50);
      drawNumber(9, 35, 50);
    }

    // Extract each digit of the number and draw them
    while ( score > 0 && score < 100 ) {
      
      num = score % 10;
      score = score / 10;

      drawNumber(num, 45-xOffset, 50);
  
      xOffset += 10; 
    }
    
  }
  
}

void drawNumber(int num, int x, int y) {

  // Copy points into new array, adding the offset
  byte numToDraw[11][2];
  for ( int i = 0; i < 11; i++ ) {
    numToDraw[i][0] = numbers[num][i][0]+x;
    numToDraw[i][1] = numbers[num][i][1]+y;
  }

  drawShape(numToDraw, sizeof(numToDraw)/sizeof(numToDraw[0]));
  
}

void printScores() {
  Serial.println(player1Score);
  Serial.println(player2Score);
}

void drawShape(byte points[][2], int numPoints) {

  // Iterating many times for each draw cycle provides a persistence of vision style effect for lines we want to be visible
  for(int j = 0; j < SHAPE_ITERATIONS; j++) {
    // Plot all the points of the shape
    for( int i = 0; i < numPoints; i++) {
      movePoint(points[i][0], points[i][1]); // Plot a line to the next point
    }
  }
}

void moveX(byte x) {

  if (x > 63 ) x = 63;    // Limit magnitude of x so it doesn't overflow outside 6 bits

  PORTD = (x << 2) | (PIND & B00000011);  // Assign port D with the value, leaving the 2 LSBs untouched
  
}

void moveY(byte y) {
  
  if (y > 63 ) y = 63;  // Limit magnitude of y so it doesn't overflow outside 6 bits

  PORTB = (y) | (PINB & B11000000); // Assign port D with the value, leaving the 2 MSBs untouched
  
}

void movePoint(byte x, byte y) {

  moveX(x);
  moveY(y);
  
}
